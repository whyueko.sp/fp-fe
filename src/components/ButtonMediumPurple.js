import React from "react";
// import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';

const ButtonMediumPurple = (props) => {
  const { namaButton, linkHref, onClick } = props;

  return (
    <button className="btn btnPurpleMd mt-4" type="submit" href={linkHref} onClick={onClick}>
      {namaButton}
    </button>
  )
};

export default ButtonMediumPurple;