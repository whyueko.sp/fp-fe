import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Navbar from "react-bootstrap/Navbar";
import logoSc from '../assets/img/logo.svg';
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Container from 'react-bootstrap/Container';
// import iconListPurple from "../assets/img/iconListPurple.svg";
import iconList from "../assets/img/iconList.svg";
import iconNotif from "../assets/img/iconNotif.svg";
import iconUser from "../assets/img/iconUser.svg";

const NavbarNotif = (props) => {
  return (
    
    <>
    <Navbar expand="lg" className="shadowBox">
        <Container>
          <div className='logoNavbarNotif'>
            <Navbar.Brand href="/"><img src={logoSc} alt="logo" className='logoNavbarNotif'/></Navbar.Brand>
          </div>

          <Nav>
              <Form className="kotakCari">
                <Form.Control
                  type="search"
                  placeholder="Cari di sini ..."
                  className="searchBox me-2"
                  aria-label="Search"
                  style={{width:'115%'}}
                />
              </Form>
            </Nav>

          <Navbar.Toggle type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" aria-expanded="false" aria-label="Toggle navigation" />

          <Navbar.Collapse className="offcanvas offcanvas-end justify-content-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasTopLabel">
          <div className="offcanvas-body navbarKonten">
            <Nav.Link href="/DaftarJualProduk">
              <img src={iconList} alt="icon" className="menuIcon" />
              <p className="menuText">Daftar Jual</p>
            </Nav.Link>
            <Nav.Link href="#home">
              <img src={iconNotif} alt="icon" className="menuIcon" />
              <p className="menuText">Notifikasi</p>
            </Nav.Link>
            <Nav.Link href="#home">
              <img src={iconUser} alt="icon" className="menuIcon" />
              <p className="menuText">Akun</p>
            </Nav.Link>
            </div>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>

  )
};

export default NavbarNotif; 