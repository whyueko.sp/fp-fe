import React from "react";
// import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';

const ButtonSmallPurple = (props) => {
  const { namaButton, linkHref } = props;

  return (
    <button className="btn btnPurpleSm mt-4" type="submit" href={linkHref}>
      {namaButton}
    </button>
  )
};

export default ButtonSmallPurple;