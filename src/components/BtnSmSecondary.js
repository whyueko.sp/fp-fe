import React from "react";
// import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';

const BtnSmSecondary = (props) => {
  const { namaButton, linkHref, type } = props;

  return (
    <a className="btn btnSecondarySm" type={type} href={linkHref}>
      {namaButton}
    </a>
  )
};

export default BtnSmSecondary;