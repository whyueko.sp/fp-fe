import React from "react";
// import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import iconMoney from '../assets/img/iconMoney.svg';
import iconBox from '../assets/img/iconBox.svg';
import iconHeart from '../assets/img/iconHeart.svg';

const MenuSeller = (props) => {

  return (
    <div className="menuSellerMobile">
    <div style={{ display:"flex" }} className="menuSellerMobile">
          <a className="btn btnMenuSeller" href="/daftarjualproduk">
              <img src={iconBox} alt="icon"/>     Semua Produk
          </a>
          <a className="btn btnMenuSeller" href="/daftarjualdiminati">
              <img src={iconHeart} alt="icon"/>     Diminati
          </a>
          <a className="btn btnMenuSeller" href="/daftarjualterjual">
              <img src={iconMoney} alt="icon"/>     Terjual
          </a>
      </div>
  </div>
  )
};

export default MenuSeller;