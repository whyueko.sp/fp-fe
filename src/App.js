import React from "react";
import { Routes, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import InfoProfilPage from "./pages/InfoProfilPage";
import InfoProdukPage from "./pages/InfoProdukPage";
import InfoPenawarPage from "./pages/InfoPenawarPage";
import InfoPenawar2Page from "./pages/InfoPenawar2Page";
import DaftarJualProdukPage from "./pages/DaftarJualProdukPage";
import DaftarJualDiminatiPage from "./pages/DaftarJualDiminatiPage";
import DaftarJualTerjualPage from "./pages/DaftarJualTerjualPage";
import HomePage from "./pages/HomePage";
import SellHalamanProduk from "./pages/SellHalamanProduk";
import SellHalamanProdukBuyer from "./pages/SellHalamanProdukBuyer";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/product" element={<SellHalamanProduk />} />
        <Route path="/productbuy" element={<SellHalamanProdukBuyer />} />
        <Route exact path="/Masuk" element={<LoginPage />}></Route>
        <Route exact path="/Daftar" element={<RegisterPage />}></Route>
        <Route exact path="/LengkapiProfil" element={<InfoProfilPage />}></Route>
        <Route exact path="/LengkapiProduk" element={<InfoProdukPage />}></Route>
        <Route exact path="/InfoPenawar" element={<InfoPenawarPage />}></Route>
        <Route exact path="/InfoPenawar2" element={<InfoPenawar2Page />}></Route>
        <Route exact path="/DaftarJualProduk" element={<DaftarJualProdukPage />}></Route>
        <Route exact path="/DaftarJualDiminati" element={<DaftarJualDiminatiPage />}></Route>
        <Route exact path="/DaftarJualTerjual" element={<DaftarJualTerjualPage />}></Route>
      </Routes>
    </>
  );
}

export default App;
