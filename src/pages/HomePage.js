import Carousel from "react-bootstrap/Carousel";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import NavbarHome from '../components/NavbarHome';
import carousel1 from "../assets/img/imgbanner.png";
import card1 from "../assets/img/card.png";
import iconSearch from "../assets/img/fi_search.png";
import gift from "../assets/img/gift.png";
import "../style.css";
import "../Global.css";


function HomePage() {
  return (
    <>
      <div className="smHomeSlider">
        {/* NAVBAR */}
        <NavbarHome />
        <Container className="d-block d-sm-none pt-4">
          <div class="row">
            <div class="col">
              <h2>
                <b>Bulan Ramadhan Banyak Diskon!</b>
              </h2>
              <br></br>
              <h6>Diskon Hingga</h6>
              <h4 style={{ color: "red" }}>60 %</h4>
            </div>
            <div class="col">
              <img src={gift} alt="gift"></img>
            </div>
          </div>
        </Container>
        {/* CAROUSEL */}
        <Container className="mt-4 ">
          <Carousel className="d-none d-sm-block">
            <Carousel.Item>
              <img
                className="d-none d-sm-block w-100"
                src={carousel1}
                alt="First slide"
              />
              <Carousel.Caption>
                <p></p>
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
              <img
                className="d-none d-sm-block w-100"
                src={carousel1}
                alt="Second slide"
              />
              <Carousel.Caption>
                <p></p>
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
              <img
                className="d-none d-sm-block w-100"
                src={carousel1}
                alt="Third slide"
              />
              <Carousel.Caption>
                <p></p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </Container>

        {/* BARIS */}
        <Container>
          <br></br>
        </Container>
      </div>
      {/* TELUSURI KATEGORI */}
      <Container>
        <h5>Telusuri Kategori</h5>

        <Row xs="auto">
          <div style={{ display:"flex" }} className="menuKategori">
          
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Semua
            </Button>
          
          
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Hobi
            </Button>
          
          
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Kendaraan
            </Button>
          
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Baju
            </Button>
         
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Elektronik
            </Button>
          
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Kesehatan
            </Button>
          
          </div>
        </Row>
        
      </Container>

      {/* BARIS */}
      <Container>
        <br></br>
      </Container>

      {/* CARD */}
      <Container>
        <Row xs={2} md={3} lg={6} className="g-4">
          {Array.from({ length: 12 }).map((_, idx) => (
            <Col>
              <Card>
                <Card.Img variant="top" className="p-2" src={card1} />
                <Card.Body>
                  <Card.Title className="h1Card">
                    <b>Jam Tangan Casio</b>
                  </Card.Title>
                  <Card.Subtitle className="h2Card">Aksesoris</Card.Subtitle>
                  <Card.Text className="h3Card">Rp 250.000</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
      <Container>
        <Button
          className="btnJual"
          href="/lengkapiProduk"
        >
          + Jual
        </Button>
      </Container>
    </>
  );
} 

export default HomePage;
