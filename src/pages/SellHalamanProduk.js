import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Carousel from "react-bootstrap/Carousel";
import InfoProduct1 from "../assets/img/InfoProduct.png";
import iconSeller1 from "../assets/img/iconSeller.png";
import NavbarNotif from '../components/NavbarNotif';


function SellHalamanProduk() {
  return (
    <>
      {/* NAVBAR */}
      <NavbarNotif/>

      {/* PRODUCT */}
      <Container>
        <div class="row mt-4">
          <div class="col-lg-8">
            <Carousel>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={InfoProduct1}
                  alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={InfoProduct1}
                  alt="Second slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={InfoProduct1}
                  alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
          </div>
          {/* COL KE 2 */}
          <div class="col-lg-4">
            <div class="textShadowBox p-4">
              <h4>Jam Tangan Casio</h4>
              <h6>Aksesoris</h6>
              <h5>Rp 250.000</h5>
              <Button className="btnPurple w-100 mt-2 mb-2">Terbitkan</Button>
              <Button
                className="btnPurple w-100 mt-2 "
                style={{ background: "#FFFFFF", color: "black" }}
              >
                Edit
              </Button>
            </div>

            <div class="textShadowBox p-4 mt-4">
              <div className="row justify-content-start">
                <div className="col-2">
                  <img src={iconSeller1} alt="iconseller" />
                </div>
                <div className="col-8">
                  <h5>Nama penjual</h5>
                  <h6>Kota</h6>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* TEXT DESCRIPSI */}
        <div class="row">
          <div class="col-lg-8">
            <div class="textShadowBox p-4 mt-4 mb-4">
              <h4>Deskripsi</h4>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
}

export default SellHalamanProduk;
