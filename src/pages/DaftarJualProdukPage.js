import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
// import ButtonMediumPurple from '../components/ButtonMediumPurple';
// import BtnMdSecondary from '../components/BtnMdSecondary';
// import NavbarPlain from '../components/NavbarPlain';
import NavbarNotif from '../components/NavbarNotif';
import buyer from '../assets/img/buyer.svg';
import jamTangan1 from '../assets/img/jamTangan1.svg';
import BtnSmSecondary from '../components/BtnSmSecondary';
import ArrowLeft from '../assets/img/arrowLeft.svg';
import ArrowLeftPurple from '../assets/img/arrowLeftPurple.svg';
import iconBoxPurple from '../assets/img/iconBoxPurple.svg';
import iconMoney from '../assets/img/iconMoney.svg';
import iconHeart from '../assets/img/iconHeart.svg';
import tambahProduk from '../assets/img/tambahProduk.svg';
import MenuSeller from '../components/MenuSeller';

function DaftarJualProdukPage() {
    return (
      <>
       
        {/* NAVBAR */}
        <NavbarNotif/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            {/* Title */}
            <Row>
              <Col md={4}>
                <div className='titleDaftarJual'>
                    <h4><b>Daftar Jual Saya</b></h4>
                </div>
              </Col>
            </Row>
          
             {/* Card Penjual */}
            <Row>
              <Col lg={12}>
                  <Container>
                    <Card className='cardSeller'>
                        <Card.Body>
                        <div className="kontenCardSeller">
                        <Row>
                            <Col>
                                <img src={ buyer } alt="" id="fotoSeller"/>
                            </Col>
                            <Col>
                              <div className="txtCardSeller">
                                <p><b>Nama Penjual</b></p>
                                <p>Kota</p>
                              </div>
                            </Col>
                            <Col className="btnEditSeller mt-3">
                            <BtnSmSecondary namaButton="Edit" type="button "linkHref="/lengkapiProfil"/>
                            </Col>
                        </Row>
                        </div>
                        </Card.Body>
                    </Card>
                  </Container>
                </Col> 
            </Row>

            {/* Menu Penjual */}
            <Container className='pageDaftarJual'>
            <Row>
       
              <Col lg={3}>
                <Card className='cardMenuPenjual'>
                  <div className='kontenCardMenuPenjual'>
                      <p><b>Kategori</b></p>
                        <p>
                          <img src={iconBoxPurple} alt="icon"/>
                          <a href="/DaftarJualProduk" className="txtMenuPenjual" id="menuPressed"><b> Semua Produk      </b>  </a>        
                          <img src={ArrowLeftPurple} alt="iconArrow"/>
                        </p>
                      <hr/>
                      <p>
                          <img src={iconHeart} alt="icon"/>
                          <a href="/DaftarJualDiminati" className="txtMenuPenjual">  Diminati     </a>        
                          <img src={ArrowLeft} alt="iconArrow"/>
                      </p>
                      <hr/>
                      <p>
                        <img src={iconMoney} alt="icon"/>
                        <a href="/DaftarJualTerjual" className="txtMenuPenjual">  Terjual     </a>        
                        <img src={ArrowLeft} alt="iconArrow"/>
                      </p>
                  </div>
                </Card>

                {/* for smaller device (menu button) */}
                <MenuSeller/>
                
              </Col>

                {/* Produk */}
                <Col lg={9} className='colJual'>
                  <Row xs={2} md={3} className="g-4 kontenProduk">
                    <a href="/LengkapiProduk">
                      <img src={tambahProduk} alt="icon" className='imgTambahProduk'/>
                    </a>
                    {Array.from({ length: 12 }).map((_, idx) => (
                      <Col>
                        <Card className='txtCardProduk'>
                          <Card.Img variant="top" src={jamTangan1} />
                          <Card.Body className='txtCardProduk'>
                            <p className="h1Card"><b>Jam Tangan Casio</b></p>
                            <p className="h2Card">Aksesoris</p>
                            <p className="h1Card">Rp 250.000</p>
                          </Card.Body>
                        </Card>
                      </Col>
                    ))}
                  </Row>
               </Col>




               </Row>
            </Container>

        </Container>
      </>
    );
  };

  export default DaftarJualProdukPage;