import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import ButtonLargePurp from '../components/ButtonLargePurp';
// import BtnMdSecondary from '../components/BtnMdSecondary';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import buyer from '../assets/img/buyer.png';
import jamTangan1 from '../assets/img/jamTangan1.svg';
import ButtonMediumPurple from '../components/ButtonMediumPurple';
import BtnMdSecondary from '../components/BtnMdSecondary';
import Modal from "react-bootstrap/Modal";
import { Radio, RadioGroup } from 'react-radio-group'

function ModalStatusPenjualan(props) {
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="modalBox"
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body style={{marginLeft:'5%', marginRight:'5%', marginBottom:'3%'}}>
        <h6> <b>Perbarui status penjualan produkmu</b> </h6>
        {/* modal status*/}
        <div>
          <Form style={{paddingTop:'6%'}}> 
            <RadioGroup name="transaksi" onChange={(e) => this.handleOnChange(e)}>
              <div className="radio-button-background">
                  <Radio value="Berhasil" className="radioButton" />   Berhasil terjual
              </div>
              <p className='expCheckBox'>Kamu telah sepakat menjual produk ini kepada pembeli</p>

              <div className="radio-button-background">
                  <Radio value="Batal" className="radioButton" />   Batalkan transaksi
              </div>
              <p className='expCheckBox'>Kamu membatalkan transaksi produk ini dengan pembeli</p>
            </RadioGroup>
          </Form>
        </div>
        <ButtonLargePurp onClick={props.onHide} type="submit" namaButton="Kirim"/>
      </Modal.Body>
  </Modal>
  );
}


function InfoPenawar2Page() {
  const [modalShow, setModalShow] = React.useState(false);

    return (
      <>
       
        {/* NAVBAR */}
        <NavbarPlain judulPage="Info Penawar"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <div className='iconArrowLeft'>
                    <ArrowLeft size={34} className='' href='/'/>
                </div>
              </Col>
            </Row>
          
            {/* Card Penawar */}
            <Row>
              <Col lg={12}>
                  <Container>
                    <Card className='cardPenawar'>
                        <Card.Body>
                        <div className="kontenCardPenawar">
                        <Row>
                            <Col>
                                <img src={ buyer } alt="" id="fotoBuyer"/>
                            </Col>
                            <Col>
                              <div className="txtCardPenawar">
                                <p><b>Nama Pembeli</b></p>
                                <p>Kota</p>
                              </div>
                            </Col>
                        </Row>
                        </div>
                        </Card.Body>
                    </Card>
                  </Container>
                </Col> 
            </Row>

            {/* Daftar Produk */}
            <Row>
              <Col lg={12}>
                <Container>
                  <p className="titleTawar"><b>Daftar produkmu yang ditawar</b></p>

                  <Card className="float-right cardBarangDitawar">
                    <Row>
                      <Col sm={5}>
                        <img className='imgBarangDitawar' src={ jamTangan1 } alt=""/>
                      </Col>
                      <Col sm={7}>
                        <div className="kontenCardBarangDitawar">
                            <p>27 Sept, 19:16 WIB</p>
                            <h6 className="card-title"><b>Jam Tangan Casio</b></h6>
                            <p>Rp 250.000</p>
                            <p><b>Ditawar Rp 200.000</b></p>
                            <BtnMdSecondary namaButton="Status" linkHref="/"/>
                            <ButtonMediumPurple namaButton="Hubungi" onClick={() => setModalShow(true)} linkHref="/"/>
                            <ModalStatusPenjualan show={modalShow} onHide={() => setModalShow(false)} />
                        </div>
                     </Col>
                    </Row>
                  </Card>

                </Container>
              </Col>
            </Row>

        </Container>
      </>
    );
  };

  export default InfoPenawar2Page;