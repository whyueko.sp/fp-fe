import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import '../Global.css';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonLargePurp from '../components/ButtonLargePurp';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import inputPhotoProfile from '../assets/img/inputPhotoProfile.svg';

function InfoProfilPage() {

  // Implement react-dropzone for collecting photo

     const img = { display: 'block', width: 'auto', height: '100%' };

     const [files, setFiles] = useState([]);
     const {getRootProps, getInputProps} = useDropzone({
       accept: {
         'image/*': []
       },
       onDrop: acceptedFiles => {
         setFiles(acceptedFiles.map(file => Object.assign(file, {
           preview: URL.createObjectURL(file)
         })));
       }
     });
     
     const thumbs = files.map(file => (
       <div className='thumb' key={file.name}>
         <div className='thumbInner'>
           <img src={file.preview} style={img} alt="productImgNew"
             onLoad={() => { URL.revokeObjectURL(file.preview) }}
           />
         </div>
       </div>
     ));
   
     useEffect(() => {
       return () => files.forEach(file => URL.revokeObjectURL(file.preview));
     }, []);

  // page

    return (
      <>
        {/* NAVBAR */}
        <NavbarPlain judulPage="Lengkapi Info Akun"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <a className='iconArrowLeft' href="/DaftarJualProduk" style={{color:"black"}}>
                    <ArrowLeft size={34} className='' href='/'/>
                </a>
              </Col>
            </Row>

            {/* Form Lengkapi Profil */}
            <Row>
              <Col lg={4}></Col>
              <Col lg={4} md={6}>
                <div className='formInfoProfil'>
                <Form className='mt-md-6 mb-4' style={{paddingBottom: '22%'}} action="" method="" enctype="">
                        <Form.Group className="mb-3 mt-4" controlId="formBasicProfilePhoto">
                             <div {...getRootProps({className: 'dropzone'})}>
                                <input {...getInputProps()} />
                                  <img src={inputPhotoProfile} alt="foto" style={{position:'relative', left:'38%'}}/>
                              </div>
                              <aside className='thumbsContainer' style={{position:'relative', left:'38%'}}>
                                {thumbs}
                              </aside>
                        </Form.Group>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                          <Form.Label>Nama</Form.Label>
                          <Form.Control type="text" id="name" placeholder="Nama" className="formRounded" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicCity">
                          <Form.Label>Kota</Form.Label>
                          <Form.Select type="text"  id="kategoriProduk" placeholder="Pilih Kota" className="formRounded" required>
                            <option value="" selected>- Pilih Kota -</option>
                            <option value="1">Aceh</option>
                            <option value="2">Jakarta</option>
                            <option value="3">Surabaya</option>
                            <option value="4">Bali</option>
                          </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicAddress">
                          <Form.Label>Alamat</Form.Label>
                          <Form.Control type="textarea"  id="alamat" placeholder="Masukan alamat" className="formRounded inputLarge" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicHP">
                          <Form.Label>Nomer Handphone</Form.Label>
                          <Form.Control type="text"  id="noHP" placeholder="Masukan no HP" className="formRounded" required />
                        </Form.Group>

                        <ButtonLargePurp namaButton="Simpan" linkHref="/"/>
                  </Form>
                  </div>
                </Col>
                <Col md={4}></Col>
            </Row>
        </Container>
      </>
    );
  };

  export default InfoProfilPage;