import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Form from 'react-bootstrap/Form';
import coverSecondHand from '../assets/img/coverSecondHand.png';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SecondHand from '../assets/img/SecondHand.svg'
import ButtonLargePurp from '../components/ButtonLargePurp';
import { ArrowLeft } from 'react-bootstrap-icons';

function LoginPage() {
  return (
    <>
      {/* KONTEN */}
      <Container fluid>
          <Row>

              {/* Foto */}
              <Col md={6}>
                  <div id='overlay'>
                    <img src={ SecondHand } alt="" id="secondHand"/>
                    <img src={ coverSecondHand } alt="" id="imgRegister"/>
                  </div>
              </Col>

              {/* Form Login atau Masuk */}
              <Col md={6} className="mt-lg-5">
                <Container className="loginText">

                  <a className='iconArrowRegis' href='/' style={{color:'black'}}>
                        <ArrowLeft size={34} className='' />
                  </a>

                  <h3 className='mb-lg-4'><b>Masuk</b></h3>
                  <Form action="" method="" enctype="">
                      <Form.Group className="mb-3 mt-4" controlId="formBasicEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" id="email" placeholder="Contoh: johndee@gmail.com" className="formRounded" required />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password"  id="password" placeholder="Masukan password" className="formRounded" required />
                      </Form.Group>
                      <ButtonLargePurp namaButton="Masuk" linkHref="/"/>
                      <p className="fw-bold mt-2 pt-4 mb-0 txtRegister">Belum punya akun?<a href="/Daftar" className="btnLinkPurp">  Daftar di sini</a></p>
                    </Form>
                </Container>
              </Col>

          </Row>
      </Container>
    </>
  );
};

export default LoginPage;

// <buttonLargePurple namaButton="Test"/>