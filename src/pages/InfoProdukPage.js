import React from 'react';
import { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonMediumPurple from '../components/ButtonMediumPurple';
import BtnMdSecondary from '../components/BtnMdSecondary';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import inputPhotoProduk from '../assets/img/inputPhotoProduk.svg';

function InfoProdukPage() {

   // Implement react-dropzone for collecting photo

        const img = { display: 'block', width: 'auto', height: '100%' };

        const [files, setFiles] = useState([]);
        const {getRootProps, getInputProps} = useDropzone({
          accept: {
            'image/*': []
          },
          onDrop: acceptedFiles => {
            setFiles(acceptedFiles.map(file => Object.assign(file, {
              preview: URL.createObjectURL(file)
            })));
          }
        });
        
        const thumbs = files.map(file => (
          <div className='thumb' key={file.name}>
            <div className='thumbInner'>
              <img src={file.preview} style={img} alt="productImgNew"
                onLoad={() => { URL.revokeObjectURL(file.preview) }}
              />
            </div>
          </div>
        ));
      
        useEffect(() => {
          return () => files.forEach(file => URL.revokeObjectURL(file.preview));
        }, []);

  // page

    return (
      <>
        {/* NAVBAR */}
        <NavbarPlain judulPage="Lengkapi Info Produk"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <a className='iconArrowLeft' href="/DaftarJualProduk" style={{color:"black"}}>
                    <ArrowLeft size={34} className='' href='/'/>
                </a>
              </Col>
            </Row>
          
            {/* Form Info Produk */}
            <Row>
              <Col lg={4}></Col>
              <Col lg={4} md={6}>
                <div className='formInfoProduk'>
                <Form className='mt-md-6' style={{paddingBottom: '22%'}} action="" method="post" enctype="">
                        <Form.Group className="mb-3 mt-4" controlId="formBasicProd">
                          <Form.Label>Nama Produk</Form.Label>
                          <Form.Control type="text" id="namaProduk" placeholder="Nama Produk" className="formRounded" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPrice">
                          <Form.Label>Harga Produk</Form.Label>
                          <Form.Control type="text" id="hargaProduk" placeholder="Rp 0,00" className="formRounded" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicCategory">
                          <Form.Label>Kategori</Form.Label>
                          <Form.Select type="text"  id="kategoriProduk" placeholder="Pilih Kategori" className="formRounded" required>
                            <option value="1">Elektronik</option>
                            <option value="2">Pakaian</option>
                            <option value="3">Alat Olahraga</option>
                            <option value="4">Alat Musik</option>
                            <option value="5">Mainan</option>
                            <option value="" selected>Pilih Kategori</option>
                          </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicDesc">
                          <Form.Label>Deskripsi</Form.Label>
                          <Form.Control type="textarea"  id="deskripsiProduk" placeholder="Tulis deskripsi produk" className="formRounded inputLarge" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicProductPhoto">
                          <Form.Label>Foto Produk<br/></Form.Label>
                            
                              <div {...getRootProps({className: 'dropzone'})}>
                                <input {...getInputProps()} />
                                <img src={inputPhotoProduk} alt="foto"/>
                              </div>
                              <aside className='thumbsContainer'>
                                {thumbs}
                              </aside>
                           

                        </Form.Group>
                      
                        <div>
                            <BtnMdSecondary namaButton="Preview" linkHref="/"/>
                            <ButtonMediumPurple namaButton="Terbitkan" linkHref="/"/>
                        </div>
                  </Form>
                  </div>
                </Col>
                <Col md={4}></Col>
            </Row>
        </Container>
      </>
    );
  };

export default InfoProdukPage;